//console.log("Hello World");

// [SECTION] While loop
/*
	- A while loop takes in an expression/condition
	- Expressions are any unit of code that can evaluated to a value
*/

let count = 5;

while(count!==0){
	console.log("count: "+ count);
	count--; //decrements countvalue //count = count -1;
}

// [SECTION] Do while

/*let number = Number(prompt("Give me a number: "));
// The "Number" function works similar to the "parseInt" function.


// After executing once, the while statement will evaluate whether to run the next iteration of the loop based on given expression/condition (e.g. number less than 10)
// If the expression/condition is not true (false), the loop will stop.
do {
	console.log("Number: "+ number);
	number += 1; //increments value into 1
}
while(number < 10); //number must be less than 10*/

/*let n = 15;

while(number < 10){
	console.log("Number: "+ number);
	number ++;
}*/

// [SECTION] For loop
	// initialization //condition //change of value
for(let count = 10; count <= 20; count++){
	console.log(count);
};
			
let myString = "tupe";
// t = index 0
// u = index 1
// p = index 2
// e = index 3

console.log("myString length: "+ myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(i=0; i < myString.length; i++){
	console.log(myString[i]);
}

let myName = "AlEx";

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 	
	){					
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}